// ==========================Resizing===================================//
interact('.resize-drag')
.draggable({
  onmove: window.dragMoveListener,
  restrict: {
    restriction: 'parent',
    elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
  },
})
.resizable({
  edges: { left: true, right: true, bottom: true, top: true },

  restrictEdges: {
    outer: 'parent',
    endOnly: true,
  },
  restrictSize: {
    min: { width: 80, height: 80 },
    max: { width: 130, height: 130},
  },

  inertia: true,
})
.on('resizemove', function (event) {
  var target = event.target,
      x = (parseFloat(target.getAttribute('data-x')) || 0),
      y = (parseFloat(target.getAttribute('data-y')) || 0);

  target.style.width  = event.rect.width + 'px';
  target.style.height = event.rect.height + 'px';

  x += event.deltaRect.left;
  y += event.deltaRect.top;

  target.style.webkitTransform = target.style.transform =
      'translate(' + x + 'px,' + y + 'px)';
}); 
// ===========================Resizing End==================================//

// ==========================Draggable===================================//
  interact('.draggable')
  .draggable({
    inertia: true,
    restrict: {
      restriction: 'parent',
      endOnly: true,
      elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    },
    autoScroll: true,

    onmove: dragMoveListener,
  });

  function dragMoveListener (event) {
    var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
  }

  window.dragMoveListener = dragMoveListener;
// ==========================Draggable===================================//

  // ==========================Clonar===================================//
 
  var clickIcon =  [].slice.call(document.querySelectorAll('.img-decor'));
					 
				     clickIcon.forEach(function (element, i){
						element.addEventListener('click', function (e){
							e.preventDefault();
						var Copy = $(element).first().clone().removeClass('icon-decor img-decor').addClass('resize-drag icon-onStage').appendTo(".stage");
						 
						 });
					});
// ==========================Clonar===================================//
