// Panel Menu
var btnp = document.getElementsByClassName("btn-panel");
var i;

for (i = 0; i < btnp.length; i++) {
    btnp [i].addEventListener("click", function() {
        this.classList.toggle("active");

        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
} 
//Fin Panel Menu

